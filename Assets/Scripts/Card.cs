﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
[CreateAssetMenu(fileName = "New Card", menuName = "Cards/DeckCard")]
public class Card : ScriptableObject
{
    public CardType cardType;
    public CardValue cardValue;
    public Sprite cardSprite;
}

public enum CardType {
    none,
    heart,
    diamond,
    spades,
    clubs
}

public enum CardValue {
    none,
    ace,
    _2,
    _3,
    _4,
    _5,
    _6,
    _7,
    _8,
    _9,
    _10,
    jack,
    quenn,
    king
}
