﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameConditions : Singleton<GameConditions> {

    public void ConditionCheck (Card card, Character character) {
        if (Desk.Instance.accumulatedCards.Count < 2)
            return;

        Card uppermostCard = Desk.Instance.accumulatedCards [Desk.Instance.accumulatedCards.Count - 2];
        List<Card> accumulatedCards = Desk.Instance.accumulatedCards;
        if(Desk.Instance.baseCard)

        if (card.cardValue == CardValue.jack) {
            character.AddPoint (20 + CalculatePoints (accumulatedCards));
            character.CollectCard ();
            GameEvents.Instance.OnPistid.Invoke ();
            } else if (card.cardValue == uppermostCard.cardValue) {
            character.AddPoint (10 + CalculatePoints (accumulatedCards));
            character.CollectCard ();
            GameEvents.Instance.OnPistid.Invoke ();
            }

    }

    public int CalculatePoints (List<Card> cards) {
        int point = 0;
        foreach (var item in cards) {
            if (item.cardType == CardType.clubs && item.cardValue == CardValue._2)
                point += 2;
            else if (item.cardType == CardType.diamond && item.cardValue == CardValue._10)
                point += 3;
            else if (item.cardValue == CardValue.ace || item.cardValue == CardValue.jack)
                point += 1;
        }

        return point;
    }

    public int CalculatePoint (Card card) {
        int point = 0;
 
        if (card.cardType == CardType.spades && card.cardValue == CardValue._2)
            point = 2;
        else if (card.cardType == CardType.diamond && card.cardValue == CardValue._10)
            point = 3;
        else if (card.cardValue == CardValue.ace || card.cardValue == CardValue.jack)
            point = 1;
        
        return point;
    }

    public void GetMoreCardPoint () {
        Character whoHasMoreCard = CharacterManagementSystem.Instance.characters [0];
        foreach (var item in CharacterManagementSystem.Instance.characters) {
            if (item.collectedCards.Count > whoHasMoreCard.collectedCards.Count)
                whoHasMoreCard = item;
        }
        whoHasMoreCard.AddPoint (3);
    }

}
