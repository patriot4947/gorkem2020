﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class GameEvents : Singleton<GameEvents>
{
    internal UnityEvent OnCardRunOutOffedTheirHand = new UnityEvent();
    internal UnityEvent OnPistid = new UnityEvent ();
    internal UnityEvent OnGameFinished = new UnityEvent ();
    internal UnityEvent OnPlayerPlayedCard = new UnityEvent ();
    // Start is called before the first frame update
    void Start()
    {
        OnCardRunOutOffedTheirHand.AddListener (OnCardRunOutOfTheirHand);
        OnGameFinished.AddListener (OnGameFinish);
        OnPlayerPlayedCard.AddListener (OnPlayerPlayCard);
        OnPistid.AddListener (OnPisti);
    }

    public void OnCardRunOutOfTheirHand () {
        LogManager.Instance.PrintAllLogs ();
        if (!Deck.Instance.cardDeck.Any ())
            OnGameFinished.Invoke ();
        else {
            foreach (var item in CharacterManagementSystem.Instance.characters) {
                 item.GetCards (Deck.Instance.GetNextCards (GameManager.Instance.willBeDistrubutedCardAmount));
            }
        }
    }

    public void OnGameFinish () {
        CharacterManagementSystem.Instance.lastCollectedChracter.CollectCard ();
        GameConditions.Instance.GetMoreCardPoint ();

        GameObject gameOverPanel = Instantiate (GameManager.Instance.gameOverPanel, GameManager.Instance.mainCanvas.transform);
        gameOverPanel.GetComponent<GameOverPanel> ().SetPlayerScoreText ("YourScore: " + CharacterManagementSystem.Instance.GetMainPlayer().point);
        gameOverPanel.GetComponent<GameOverPanel> ().SetAIScoreText ("AI Score: " + CharacterManagementSystem.Instance.GetAIPlayer().point);
        LogManager.Instance.PrintAllLogs ();
    }

    public void OnPlayerPlayCard () {
        Character character = CharacterManagementSystem.Instance.GetNextCharacter ();
        ComputerAI computerAI = character.GetComponent<ComputerAI> ();
        if (character.controllerType == ControllerType.AI) {
            computerAI.SelectCard (computerAI.MakeDecision ());
        }
    }

    public void OnPisti () {
        LogManager.Instance.PrintAllLogs ();
    }

}


