﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CharacterManagementSystem : Singleton<CharacterManagementSystem>
{
    //public GameObject characterPrefab;
    //public GameObject deckPanel;
    int turnNumber;
    public List<Character> characters;
    public Character lastCollectedChracter;
    // Start is called before the first frame update
    void Start()
    {
       // CreateCharacters ();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //private void CreateCharacters () {

    //    characters.Add(Instantiate (characterPrefab).AddComponent<PlayerInputBinder>().GetComponent<Character>());
    //    characters [characters.Count - 1].controllerType = ControllerType.Player;
    //    Instantiate (deckPanel, GameManager. ,Quaternion.identity,GameManager.Instance.mainCanvas.transform);

    //    characters.Add (Instantiate (characterPrefab).AddComponent<ComputerAI> ().GetComponent<Character>());
    //    characters [characters.Count - 1].controllerType = ControllerType.AI;
        
    //}


    public void RunOutOfHandCards () {
        int count = 0;
        foreach (var item in characters) {
            if (!item.handCards.Any ())
                count++;
        }

        if (count == characters.Count)
            GameEvents.Instance.OnCardRunOutOffedTheirHand?.Invoke ();
    }

    public Character GetNextCharacter () {
        turnNumber++;
        turnNumber = turnNumber % characters.Count;
        return characters [turnNumber];
    }

    public Character GetMainPlayer () {
        foreach (var item in characters) {
            if (item.controllerType == ControllerType.Player)
                return item;
        }

        return null;
    }

    public Character GetAIPlayer () {
        foreach (var item in characters) {
            if (item.controllerType == ControllerType.AI)
                return item;
        }

        return null;
    }

}
