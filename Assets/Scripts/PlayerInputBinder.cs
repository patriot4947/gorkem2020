﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerInputBinder : MonoBehaviour {
    Character character;
    // Start is called before the first frame update
    void Start()
    {
        character = GetComponent<Character>();
        OnClickCard ();
    }

    public void OnClickCard () {
        foreach (var item in character.GUIPanel.playerHandDeckPanel.cardGUIList) {
            item.button.onClick.AddListener (() => { item.GetComponent<CardGUI> ().SetUnvisibleCard();
                                                     character.PlayCard (item.GetComponent<CardGUI> ().GetCard ());
            });
            item.rightClickAction += LogManager.Instance.PrintAllLogs; 
        }

    }
}
