﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Character : MonoBehaviour
{
    internal List<Card> handCards = new List<Card> ();
    internal List<Card> collectedCards = new List<Card> ();
    public ControllerType controllerType;
    public string userName;
    public PlayerGUIPanel GUIPanel; 
    internal int point = 0;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    { 
    }

   public void PlayCard (Card card) {


        foreach (var item in handCards) {
            if (item.cardType == card.cardType && item.cardValue == card.cardValue) {
                handCards.Remove (item);
                break;
            }
        }
        //handCards.Remove(handCards.Find (x => x.cardType == card.cardType && x.cardValue == card.cardValue));

        Desk.Instance.AddAccumulatedCard (card);
        GameConditions.Instance.ConditionCheck (card, GetComponent<Character> ());


        if (handCards != null && !handCards.Any ())
            CharacterManagementSystem.Instance.RunOutOfHandCards ();

        GameEvents.Instance.OnPlayerPlayedCard.Invoke ();

    }

    public void CollectCard () {
        if (Desk.Instance.baseCard) {
            collectedCards.Add (Desk.Instance.baseCard);
            Desk.Instance.SetBaseCard ();
            AddPoint(GameConditions.Instance.CalculatePoint (Desk.Instance.baseCard));
        }
        CharacterManagementSystem.Instance.lastCollectedChracter = this;
        collectedCards.AddRange(Desk.Instance.accumulatedCards);
        Desk.Instance.CollectAllAccumulatedCards();
        GUIPanel.playerCollectectedCardsPanel.UpdateAccumulatedCard (collectedCards[collectedCards.Count-1].cardSprite);
    }

    public void GetCards (List<Card> cards) {
        GUIPanel.playerHandDeckPanel.SetEnableAllCards ();
        GUIPanel.playerHandDeckPanel.UpdateCardsContent (cards);
        handCards.AddRange (cards);
    }

    public void GetInitialCard (Card card) {
        collectedCards.Add (card);
        AddPoint (GameConditions.Instance.CalculatePoint (card));
        GUIPanel.playerCollectectedCardsPanel.UpdateAccumulatedCard (Deck.Instance.backSprite);
    }

    public void AddPoint (int point) {
        this.point += point;
        GUIPanel.SetScoreText (name + "(" + this.point.ToString () + ")" );
    }

    public void ResetPoint () {
        this.point = 0;
        GUIPanel.SetScoreText (name + "(" + this.point.ToString () + ")");
    }

}

public enum ControllerType {
    Player,
    AI,
    Network,
}
