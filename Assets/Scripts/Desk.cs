﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Desk : Singleton<Desk>
{
    [HideInInspector]
    public List<Card> accumulatedCards = new List<Card> ();
    internal Card baseCard;

    public DeskPanel deskPanel;

    public void SetBaseCard (Card card = null) {
        if (card) {
            baseCard = card;
            deskPanel.UpdateBaseCard (baseCard.cardSprite);
        } else {
            deskPanel.UpdateBaseCard ();
        }

    }

    public void AddAccumulatedCard (Card card) {
        accumulatedCards.Add (card);
        deskPanel.UpdateAccumulatedCard (card.cardSprite);
    }

    public void CollectAllAccumulatedCards () {
        accumulatedCards.Clear();
        deskPanel.UpdateAccumulatedCard ();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
