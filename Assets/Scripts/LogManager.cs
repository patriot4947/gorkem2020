﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogManager : Singleton <LogManager>
{

    public void PrintCharacterHand (Character character) {
        string log = character.userName + "_Hand = |";
        foreach (var item in character.handCards) {
            log += GetCardTypeAbbrevation (item.cardType) + "_" + GetCardValueAbbrevation (item.cardValue) + " | ";
        }
        Debug.Log (log);
    }

    public void PrintCharacterCollectedCards (Character character) {
        string log = character.userName + "_CollectedCards = |";
        for (int i = character.collectedCards.Count - 1; i >= 0; i--) {
            log += GetCardTypeAbbrevation (character.collectedCards[i].cardType) + "_" + GetCardValueAbbrevation (character.collectedCards [i].cardValue) + " | ";
        }
        Debug.Log (log);
    }

    public void PrintDeckCards () {
        string log = "Deck_Cards = |";
        foreach (var item in Deck.Instance.cardDeck) {
            log += GetCardTypeAbbrevation (item.cardType) + "_" + GetCardValueAbbrevation (item.cardValue) + " | ";
        }
        Debug.Log (log);
    }

    public void PrintAccumulatedCards () {
        string log = "Accumulated_Cards = |";

        for (int i = Desk.Instance.accumulatedCards.Count - 1; i >= 0; i--) {
            log += GetCardTypeAbbrevation (Desk.Instance.accumulatedCards[i].cardType) + "_" + GetCardValueAbbrevation (Desk.Instance.accumulatedCards[i].cardValue) + " | ";
        }

        Debug.Log (log);
    }

    public void PrintAllLogs () {
        foreach (var item in CharacterManagementSystem.Instance.characters) {
            PrintCharacterHand (item);
            PrintCharacterCollectedCards (item);
        }
        PrintDeckCards ();
        PrintAccumulatedCards ();
    }

    public string GetCardTypeAbbrevation (CardType cardType) {
        switch (cardType) {
            case CardType.clubs:
            return "C";
            case CardType.diamond:
            return "D";
            case CardType.heart:
            return "H";
            case CardType.spades:
            return "S";
            default:
            return "N";
        }
    }

    public string GetCardValueAbbrevation (CardValue cardValue) {
        switch (cardValue) {
            case CardValue.ace:
            return "A";
            case CardValue._2:
            return "2";
            case CardValue._3:
            return "3";
            case CardValue._4:
            return "4";
            case CardValue._5:
            return "5";
            case CardValue._6:
            return "6";
            case CardValue._7:
            return "7";
            case CardValue._8:
            return "8";
            case CardValue._9:
            return "9";
            case CardValue._10:
            return "10";
            case CardValue.jack:
            return "J";
            case CardValue.quenn:
            return "Q";
            case CardValue.king:
            return "K";
            default:
            return "N";
        }
    }
}


