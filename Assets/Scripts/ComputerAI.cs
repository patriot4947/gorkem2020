﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class ComputerAI : MonoBehaviour {
    Character character;
    List<CardGUI> CardGUIList = new List<CardGUI> ();

    // Start is called before the first frame update
    void Start()
    {
        character = GetComponent<Character> ();
        CardGUIList = character.GUIPanel.playerHandDeckPanel.cardGUIList;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SelectCard (Card card) {
        CardGUIList = character.GUIPanel.playerHandDeckPanel.cardGUIList;
        character.PlayCard (CardGUIList.Find (item => item.GetCard().cardType == card.cardType && item.GetCard ().cardValue == card.cardValue).GetCard ());
        // CardGUI cardGUI = CardGUIList.Find (item => item.GetCard().cardType == card.cardType && item.GetCard().cardValue == card.cardValue).cardGUI.SetDisableCard ();
        foreach (var item in CardGUIList) {
            if (item.GetCard ().cardType == card.cardType && item.GetCard ().cardValue == card.cardValue)
                item.SetUnvisibleCard ();
        }
    }

    public Card MakeDecision () {
        Card upperMostAccumulatedCard = null;
        if (Desk.Instance.accumulatedCards.Count>0)
             upperMostAccumulatedCard = Desk.Instance.accumulatedCards [Desk.Instance.accumulatedCards.Count - 1];

        Card decidedCard = ScriptableObject.CreateInstance<Card>();
        if (upperMostAccumulatedCard == null) {
            System.Random rand = new System.Random ();
            int random = rand.Next (0, character.handCards.Count);

            if (character.handCards [random].cardValue == CardValue.jack && character.handCards.Count > 1) {
                random = rand.Next (0, random);
                if (character.handCards [random].cardValue == CardValue.jack)
                    random = rand.Next (random + 1, character.handCards.Count);
            }

                decidedCard = character.handCards [random];
        } else {
            foreach (var item in character.handCards) {
                if (item.cardValue == CardValue.jack) {
                    decidedCard = item;
                    break;
                } else if (item.cardValue == upperMostAccumulatedCard.cardValue) {
                    decidedCard = item;
                    break;
                }
            }
        }

        if (decidedCard.cardType == CardType.none) {
            decidedCard = character.handCards [UnityEngine.Random.Range (0, character.handCards.Count)];
        }

        return decidedCard;
    }
    
}
