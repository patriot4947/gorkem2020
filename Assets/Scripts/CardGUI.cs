﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CardGUI : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler {
    private Card card;
    public Image image;
    public Button button;
    public Action rightClickAction;
    private Canvas canvas;
    private int orderIndex;

    public bool isClickable; //Normally, I am using OdinInspector for inspector manipulation [OnValueChanged] value but it is not free package. Therefore I don't need set these values on SetCard() Function.
    public bool isShowCard;  //

    public void SetCard (Card card) {
        this.card = card;

        if (isShowCard)
            TurnFront ();
        else
            TurnBack ();

        SetInteractableButton (isClickable);
    }

    public Card GetCard () {
        return card;
    }

    public void SetUnvisibleCard () {
        Color color = image.color;
        color.a = 0f;
        image.color = color;
        SetInteractableButton (false);
        image.raycastTarget = false;
    }

    public void SetVisibleCard () {
        Color color = Color.white;
        color.a = 1f;
        image.color = color;
        SetInteractableButton (false);
        image.raycastTarget = true;
    }

    public void SetInteractableButton (bool value) {
        button.interactable = value;
    }

    public void TurnBack () {
        image.sprite = Deck.Instance.backSprite;
    }

    public void TurnFront () {
        image.sprite = card.cardSprite;
    }

    public void OnPointerEnter (PointerEventData eventData) {
        if (!isClickable)
            return;

        canvas.sortingOrder = GameManager.Instance.willBeDistrubutedCardAmount + 1;
    }

    public void OnPointerExit (PointerEventData eventData) {
        if (!isClickable)
            return;

        canvas.sortingOrder = orderIndex;
    }

    public void OnPointerClick (PointerEventData eventData) {
        if (eventData.button == PointerEventData.InputButton.Right)
            rightClickAction?.Invoke ();
    }


    private void OnDisable () {
        canvas.sortingOrder = orderIndex;
    }

    // Start is called before the first frame update
    void Start()
    {
        canvas = GetComponent<Canvas> ();
        orderIndex = canvas.sortingOrder;
    }

}
