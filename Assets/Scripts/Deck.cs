﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Deck : Singleton<Deck>
{
    public List<Card> cardDeckItems = new List<Card> ();
    internal List<Card> cardDeck = new List<Card> ();
    public Sprite backSprite;
    // Start is called before the first frame update
    void Awake()
    {
       // ResetDeck ();
    }

    private void FillDeck () {
        cardDeck.Clear ();
        foreach (var item in cardDeckItems) {
            cardDeck.Add(item);
        }
        
    }

    private void ShuffleDeck () {
        if ((cardDeck != null) && (!cardDeck.Any ()))
            FillDeck ();

        System.Random r = new System.Random ();
        cardDeck = cardDeckItems.OrderBy (x => r.Next ()).ToList ().Select (x => (x)).ToList (); 
    }

    /// <summary>
    /// It fills and shuffles deck.
    /// </summary>
    public void ResetDeck () {
        FillDeck ();
        ShuffleDeck ();
    }


    /// <summary>
    /// Returns the next cards in the deck.(Default, it will return next one card.)
    /// </summary>
    /// <param name="amount">return card amount</param>
    public List<Card> GetNextCards (int amount = 1) {

        if (!cardDeck.Any ()) {
            Debug.LogError ("There is no any cards on the Deck");
            return null;
        }

        List<Card> nextCards = new List<Card>();

        for (int i = 0; i < amount; i++) {
            nextCards.Add (cardDeck [0]);
            cardDeck.Remove (cardDeck [0]);
        }

        return nextCards;
    }

}
