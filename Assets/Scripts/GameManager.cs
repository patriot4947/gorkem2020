﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    public readonly int willBeDistrubutedCardAmount = 4;
    internal GameObject mainCanvas;
    public GameObject gameOverPanel;
    private void Awake () {
        mainCanvas = GameObject.FindGameObjectWithTag ("MainCanvas");

    }

    // Start is called before the first frame update
    void Start()
    {
        RestartGame ();
    }


    //public void StartGame () {
    //    Deck.Instance.ResetDeck ();

    //    foreach (var item in CharacterManagementSystem.Instance.characters) {
    //        item.GetCards (Deck.Instance.GetNextCards (willBeDistrubutedCardAmount));
    //        item.GetInitialCard (Deck.Instance.GetNextCards () [0]);
    //    }

    //    Desk.Instance.SetBaseCard (Deck.Instance.GetNextCards () [0]);
    //    Desk.Instance.AddAccumulatedCard (Deck.Instance.GetNextCards () [0]);
    //}

    public void RestartGame () {
        Deck.Instance.ResetDeck ();

        foreach (var item in CharacterManagementSystem.Instance.characters) {
            item.handCards.Clear ();
            item.collectedCards.Clear ();
            item.ResetPoint ();
            item.GetCards (Deck.Instance.GetNextCards (willBeDistrubutedCardAmount));
            item.GetInitialCard (Deck.Instance.GetNextCards () [0]);
        }

        Desk.Instance.SetBaseCard (Deck.Instance.GetNextCards () [0]);
        Desk.Instance.AddAccumulatedCard (Deck.Instance.GetNextCards () [0]);
    }
}
