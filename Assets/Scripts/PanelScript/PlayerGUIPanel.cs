﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerGUIPanel : MonoBehaviour
{
    public PlayerHandDeckPanel playerHandDeckPanel;
    public PlayerCollectedCardsPanel playerCollectectedCardsPanel;
    public Text scoreText;


    public void SetScoreText (string text) {
        scoreText.text = text;
    }
}
