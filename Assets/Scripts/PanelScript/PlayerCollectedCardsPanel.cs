﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCollectedCardsPanel : MonoBehaviour
{
    public Image playerCollectedCard;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateAccumulatedCard (Sprite uppermostPlayerCollectedCard = null) {

        if (playerCollectedCard)
            playerCollectedCard.sprite = uppermostPlayerCollectedCard;
        else {
            Color color = playerCollectedCard.color;
            color.a = 0f;
            playerCollectedCard.color = color;
        }

    }

}
