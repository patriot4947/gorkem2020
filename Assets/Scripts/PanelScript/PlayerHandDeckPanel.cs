﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHandDeckPanel : MonoBehaviour
{
    public List<CardGUI> cardGUIList = new List<CardGUI> ();

    public void SetTurnBackAll () {
        foreach (var item in cardGUIList) {
            item.TurnBack ();
        }
    }

    public void SetTurnFrontAll () {
        foreach (var item in cardGUIList) {
            item.TurnFront ();
        }
    }

    public void SetEnableAllCards () {
        foreach (var item in cardGUIList) {
            item.SetVisibleCard();
        }
    }

    public void SetDisableAllCards () {
        foreach (var item in cardGUIList) {
            item.SetUnvisibleCard ();
        }
    }

    public void UpdateCardsContent (List <Card> cards) {
        for (int i = 0; i < cards.Count; i++) {
            cardGUIList [i].SetCard (cards [i]);
        }
    }


}
