﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuPanel : MonoBehaviour {
    public Button startButton;
    public Button exitButton;
    // Start is called before the first frame update
    void Start () {
        startButton.onClick.AddListener (StartGame);
        exitButton.onClick.AddListener (ExitGame);
    }

    public void StartGame () {
        SceneManager.LoadScene ("GameScene", LoadSceneMode.Single);
    }

    public void ExitGame () {
        Application.Quit ();
    }

}
