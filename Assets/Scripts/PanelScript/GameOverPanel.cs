﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverPanel : MonoBehaviour
{
    public Text playerScoreText;
    public Text aıScoreText;
    public Button restartButton;
    public Button mainMenuButton;

    void Start () {
        restartButton.onClick.AddListener (RestartGame);
        mainMenuButton.onClick.AddListener (ReturnMainMenu);
    }

    public void SetPlayerScoreText (string text) {
        playerScoreText.text = text;
    }

    public void SetAIScoreText (string text) {
        aıScoreText.text = text;
    }


    public void RestartGame () {
        Destroy (gameObject);
        GameManager.Instance.RestartGame ();
    }

    public void ReturnMainMenu () {
        SceneManager.LoadScene ("MainMenuScene", LoadSceneMode.Single);
    }

}
