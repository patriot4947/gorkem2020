﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeskPanel : MonoBehaviour
{
    public Image accumulatedCards;
    public Image baseCard;

    public void UpdateAccumulatedCard (Sprite uppermostAccumulatedCardSprite = null) {

        if (uppermostAccumulatedCardSprite) {
            accumulatedCards.sprite = uppermostAccumulatedCardSprite;
            Color color = accumulatedCards.color;
            color.a = 1f;
            accumulatedCards.color = color;

        }

        else {
            Color color = accumulatedCards.color;
            color.a = 0f;
            accumulatedCards.color = color;
        }

    }

    public void UpdateBaseCard (Sprite baseCardSprite = null) {

        if (baseCardSprite) {
            baseCard.sprite = Desk.Instance.baseCard.cardSprite;
            Color color = baseCard.color;
            color.a = 1f;
            baseCard.color = color;
        }

        else {
            Color color = baseCard.color;
            color.a = 0f;
            baseCard.color = color;
        }

    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


}
